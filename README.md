This is a custom dashboard system created using Dashing.

Some help with how to use it can be found at
https://wikis.cf.ac.uk/confluence/display/webdev/Dashboards.

You can also check out http://shopify.github.com/dashing for more
information about Dashing.
