# Example job to collect information from a website:
# Collects data with the matching CSS code.

require 'nokogiri'
require 'open-uri'

# Fetch and parse HTML document
doc = Nokogiri::HTML(open('http://blogs.cardiff.ac.uk/development/'))

titles = []
SCHEDULER.every '5m', :first_in => 0 do |job|
  all = []
  i=0
  # Search for nodes by CSS
  doc.css('main article div h1 a').each do |link|
    all[i] = { label: link.content }
    i+=1
  end
  # Number of nodes required in widget
  for i in 0..4
    titles[i] = all[i]
  end
  # Submit data to dashboard
  send_event("blog-titles", { :items => titles })
end
