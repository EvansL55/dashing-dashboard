# This job collects data from your Google Analytics.

require 'google/api_client'
require 'date'

begin
  # Update your google details in the .env file to match your own apps credentials.
  service_account_email = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_EMAIL')
  key_file = ENV.fetch('GOOGLE_KEY_FILE')
  key_secret = ENV.fetch('GOOGLE_KEY_SECRET')
  profileID = ENV.fetch('GOOGLE_PROFILEID')


  # Your application details
  client = Google::APIClient.new(:application_name => 'dashing', :application_version => '1.0')

  # Authorization setup. Do not edit.
  key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
  client.authorization = Signet::OAuth2::Client.new(
    :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
    :audience => 'https://accounts.google.com/o/oauth2/token',
    :scope => 'https://www.googleapis.com/auth/analytics.readonly',
    :issuer => service_account_email,
    :signing_key => key)

  # Start the scheduler
  SCHEDULER.every '1h', :first_in => 0 do

    # Request a token for our service account
    client.authorization.fetch_access_token!

    # Get the analytics API
    analytics = client.discovered_api('analytics','v3')

    # Analytics queries:
    # Create and test new queries on the Google Query Explorer: https://ga-dev-tools.appspot.com/query-explorer/

    previous_year_date = Date.today.prev_year.strftime # This day last year
    current_date = DateTime.now.strftime("%Y-%m-%d")  # Current date

    # Example query - Monthly Visitor Counts This Year
    monthly_visit_count = client.execute(:api_method => analytics.data.ga.get, :parameters => {
      'ids' => "ga:" + profileID,
      'start-date' => previous_year_date,
      'end-date' => current_date,
      'metrics' => "ga:uniquePageviews",
      'dimensions' => "ga:nthMonth",
      # 'sort' => "ga:nthMonth"
    })

    # Update the dashboard:

    # Monthly Visit Widget
    # Get (x,y) points for graph widget
    monthly_visit = []
    for i in 0..12
      unless defined? monthly_visit_count.data.rows[i][1]
        break
      else
        monthly_visit[i] = { x: i, y: monthly_visit_count.data.rows[i][1]}
      end
    end
    # Update the dashboard
    send_event('monthly_visit',   { points: monthly_visit })
  end
# Error handling
rescue
  puts "\e[33mMissing Google Analytics authorization! Create or update your .env file.\e[0m"
end
