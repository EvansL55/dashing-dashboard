# This job collects data from your Trello boards.

require 'trello'
include Trello

begin
  # Update these to match your own apps credentials.
  Trello.configure do |config|
    config.developer_public_key = ENV.fetch('TRELLO_DEVELOPER_PUBLIC_KEY')
    config.member_token =  ENV.fetch('TRELLO_MEMBER_TOKEN')
  end

  # Find your board ID at https://trello.com/1/members/me/boards?fields=name
  boards = {
    "my-trello-board-ID" => "12456789boardID",
  }

  # Example job:
  # Counts the statuses of cards on your trello board
  class MyTrello
    def initialize(widget_id, board_id)
      @widget_id = widget_id
      @board_id = board_id
    end

    def widget_id()
      @widget_id
    end

    def board_id()
      @board_id
    end

    def status_list()
      status = Array.new
      Board.find(@board_id).lists.each do |list|
        status.push({label: list.name, value: list.cards.size})
      end
      status
    end
  end

  @MyTrello = []
  boards.each do |widget_id, board_id|
    begin
      @MyTrello.push(MyTrello.new(widget_id, board_id))
    rescue Exception => e
      puts e.to_s
    end
  end

  SCHEDULER.every '5m', :first_in => 0 do |job|
    @MyTrello.each do |board|
      status = board.status_list()
      send_event(board.widget_id, { :items => status })
    end
  end
# Error handling
rescue
  puts "\e[33mMissing Trello authorization! Create or update your .env file.\e[0m"
end
