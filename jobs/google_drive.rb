# This job collects data from your Google Drive.

require 'google/api_client'
require "google_drive"
require 'date'

begin
  # Update your google details in the .env file to match your own apps credentials.
  service_account_email = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_EMAIL')
  key_file = ENV.fetch('GOOGLE_KEY_FILE')
  key_secret = ENV.fetch('GOOGLE_KEY_SECRET')

  # Your application details
  client = Google::APIClient.new(:application_name => 'dashing', :application_version => '1.0')

  # Authorization setup. Do not edit.
  key = Google::APIClient::KeyUtils.load_from_pkcs12(
      key_file,
      key_secret
  )
  scopes = [
    'https://docs.google.com/feeds/',
    'https://www.googleapis.com/auth/drive',
    'https://spreadsheets.google.com/feeds/'
  ]
  asserter = Google::APIClient::JWTAsserter.new(
      service_account_email,
      scopes,
      key
  )

  # Start the scheduler
  SCHEDULER.every '1h', :first_in => 0 do
    # Get your spreadsheet key from the URL of the required spreadsheet
    # https://docs.google.com/spreadsheets/d/YOUR_KEY_HERE/...
    spreadsheet_key = "abcde_spreadsheetkey"

    # Authorize access using your details
    client.authorization = asserter.authorize
    session = GoogleDrive.login_with_oauth(client.authorization.access_token)
    ws = session.spreadsheet_by_key(spreadsheet_key).worksheets[0]

    # Example job:
    # Use as inspiration or delete and start from scratch

    # Output number of columns and rows
    p "This spreadsheet has " + ws.num_cols.to_s + " columns and " + ws.num_rows.to_s + " rows."

    # Finds column headers and populates table widget with first 3 columns and 5 rows
    cols = []
    for i in 1..3
      cols << { "value" => ws[1, i] }
    end
    headers = [
      {"cols" => cols}
    ]
    data = []
    # Handle spreadsheet data
    for i in 2..5
      row = []
      k = 0
      for j in 1..3
        row[k,i-1] = { "value" => ws[i, j] }
        k+=1
      end
      data << {"cols" => row}
    end
    # Submit to widgets
    send_event('drive2',   { hrows: headers, rows: data })
  end
# Error handling
rescue
  puts "\e[33mMissing Google Drive authorization! Create or update your .env file.\e[0m"
end
