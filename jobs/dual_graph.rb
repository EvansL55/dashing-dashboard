# Example job for a Rickshawgraph with multiple series on the same axes
# For more info visit https://gist.github.com/jwalton/6614023#file-rickshawgraph-md

a = 1
b = 3
set1 = []
set2 = []
for i in 0..15
  set1 << {x: i, y: a * i}
  set2 << {x: i, y: b * i}
end

# Dual series input formatting
series = [
    {
        name: "Convergence",
        data: set1
    },
    {
        name: "Divergence",
        data: set2
    }
]
send_event('rickshaw', series: series)
