# This job collects data from your InfluxDB.

require 'influxdb'

begin
  # Update your influx details in the .env file to match your own apps credentials
  host = ENV.fetch('INFLUX_HOST')
  username = ENV.fetch('INFLUX_USERNAME')
  password = ENV.fetch('INFLUX_PASSWORD')

  # Choose a database to use
  database = 'yourdatabasename'

  influxdb = InfluxDB::Client.new database: database,
                                  host: host,
                                  username: username,
                                  password: password,
                                  :retry => 4

  # Example job:
  # Use as inspiration or delete and start from scratch

  # This job creates points for average ask prices over time to display in Graph widget

  # Initialises variables
  @influx = []
  @numPoints = 0
  @maxPoints = 0 # Maximum data values to be displayed on graph - Not used yet

  # Submit a query
  # Test new queries or find templates at yourdomain:8083

  influxdb.query 'SELECT MEAN(Ask) FROM ticker WHERE time > now() - 7d GROUP BY time(1h)' do |name, tags, points|
    for i in 1..1000
      unless defined? points[i]['time']
        # On completion do
        puts @numPoints.to_s + " values added to influx widget"
        # Set max points as the number of data values in a week
        @maxPoints = @numPoints
        break
      else
        time = points[i]['time']
        timestamp = DateTime.parse(time).to_time.to_i
        mean = points[i]['mean'].to_i
        @numPoints += 1
        @influx << { x: timestamp, y: mean }
      end
    end
  end

  # Submit event to widget
  send_event('influx', { points: @influx })

  SCHEDULER.every '1m', :first_in => '1m' do
    # Update query
    influxdb.query 'SELECT MEAN(Ask) FROM ticker WHERE time > now() - 2h GROUP BY time(1h)' do |name, tags, points|
      # Find and get the last value in the set - most recent data value
      for i in 0..15
        unless defined? points[i]['time']
          j = i-1
          time = points[j]['time']
          timestamp = DateTime.parse(time).to_time.to_i
          mean = points[j]['mean'].to_i
          if @numPoints >= @maxPoints
            @influx.shift
          else
            @numPoints += 1
          end
          @influx << { x: timestamp, y: mean }
          break
        end
      end
    end
    # Submit event to widget
    send_event('influx', { points: @influx })
  end
# Error handling
rescue
  puts "\e[33mMissing InfluxDB authorization! Create or update your .env file.\e[0m"
end
