SCHEDULER.every '1h', :first_in => 0 do
  # Calculations of percentages
  revPercent = 168
  total = 216
  revPercent = (revPercent*100/total).to_i

  counts = []
  counts << { label: "Promise", count: 7}
  counts << { label: "Fees", count: 2}
  counts << { label: "Programme Info", count: 0}
  counts << { label: "Out of date", count: 3}

  # Submit to widget
  send_event('hotmeterlist',   { items: counts, value: revPercent })
end
