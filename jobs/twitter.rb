# This job collects information from Twitter using your Twitter API app.

require 'twitter'

begin
  # Update your twitter details in the .env file to match your own apps credentials
  twitter = Twitter::REST::Client.new do |config|
    config.consumer_key = ENV.fetch('TWITTER_CONSUMER_KEY')
    config.consumer_secret = ENV.fetch('TWITTER_CONSUMER_SECRET')
    config.access_token = ENV.fetch('TWITTER_ACCESS_TOKEN')
    config.access_token_secret = ENV.fetch('TWITTER_ACCESS_TOKEN_SECRET')
  end

  search_term = URI::encode('todayilearned')

  SCHEDULER.every '10m', :first_in => 0 do |job|
    tweets = twitter.search("#{search_term}")
    comments = []
    if tweets
      tweets = tweets.map do |tweet|
        comments << { name: tweet.user.name, body: tweet.text }
      end
      send_event('twitter_mentions', { comments: comments })
    end
  end
rescue
  puts "\e[33mMissing Twitter authorization! Create or update your .env file.\e[0m"
end
