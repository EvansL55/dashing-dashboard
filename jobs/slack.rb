# This job collects data from your Slack channels.

require 'slack-ruby-client'

# Update your slack details in the .env file to match your own apps credentials.
begin
  Slack.configure do |config|
    config.token = ENV.fetch('SLACK_API_TOKEN')
  end

  # Channel, private group, or IM channel to send message to or get message from.
  # Can be an encoded ID, or a name.
  # Find channel IDs using the API https://api.slack.com/methods/channels.list/test
  channel = 'CHANNEL'

  client = Slack::Web::Client.new
  client.auth_test

  # Example job:
  # Use as inspiration or delete and start from scratch

  # Create hash of Slack user ids and corresponding usernames to display on widget
  temp = Hash.new()
  temp = client.users_list()
  if temp['ok']
    users = Hash.new()
    i = 0
    while defined? temp['members'][i]['id'] do
      users[temp['members'][i]['id']] =  temp['members'][i]['name']
      i=i+1
    end
  end

  SCHEDULER.every '10m', :first_in => 0 do |job|
    comments = Hash.new()
    # Request data from Slack: last 5 comments on a channel
    comments = client.channels_history(channel: channel, count: 5, pretty: 1)
    current = []
    if comments['ok']
      i = 0
      while defined? comments['messages'][i]['text'] do
        # Finds username from "users" for the id given in json response
        user = users[comments['messages'][i]['user']]
        # Adds username and message contents
        current << { name: user, body: comments['messages'][i]['text'] }
        i=i+1
      end
      # Submit data to widget
      send_event('slack',  comments: current)
    end
  end

# Error handling
rescue
  puts "\e[33mMissing Slack authorization! Create or update your .env file.\e[0m"
end
