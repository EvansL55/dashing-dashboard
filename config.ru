require 'sinatra/cyclist'
require 'dotenv'
Dotenv.load

require 'dashing'
configure do
  set :auth_token, ENV['AUTH_TOKEN']
  set :default_dashboard, 'sample'


  helpers do
    def protected!
     # Put any authentication code you want in here.
     # This method is run before accessing any resource.
    end
  end
end

map Sinatra::Application.assets_prefix do
  run Sinatra::Application.sprockets
end


set :routes_to_cycle_through, [:sample, :team]
# To cycle through dashboards, list your required dashboard names
# within the square brackets.
# View your pages at localhost:3030/_cycle?duration=30 - Change the
# duration to the number of seconds you wish to display each board.

run Sinatra::Application
